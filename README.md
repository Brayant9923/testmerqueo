# MerqueoTest - Android

Esta aplicación se desarrollo utilizando:
- Kotlin
- Arquitectura MVVM
- API se consume con Retrofit2
- RxJava para funciones reactivas
- Parseo con GSON
- Manejo de imágenes con Glide
- Reproducción de videos con ExoPlayer
- AndroidX
- Base de datos Room

## Capas de la aplicación
- Persistencia: Es la encargada del almacenamiento en el dispositivo, para esta capa se utilizó una clase Repositorio que abstrae el acceso a la base de datos Room.
- Red: Se encarga de consumir el API Rest que provee toda la información que se le mostrará al usuario.
- Negocio: Es la lógica que define el comportamiento de la aplicación y tratamiento de la información, usualmente la encontramos en los activities y fragments.
### MVVM Pattern
- Modelo: Se encarga de obtener, almacenar y proveer los datos además de la lógica de negocios.
- Vista: Muestra la información y reacciona ante la interacción del usuario.
- ViewModel: El el medio que utiliza la vista para acceder a los datos, a la vez, se encarga de notificar la vista cuando los datos han cambiado.
### RXJava:
- Se encarga de observar cuando hay cambios en los datos y nos evita memory leaks
###EventBus:
 - Se encarga de escuchar el llamado de adaptadores o activitys para ejecutarse
### ElegantButton
 - Se encarga de agregar o eliminar más items al carro

