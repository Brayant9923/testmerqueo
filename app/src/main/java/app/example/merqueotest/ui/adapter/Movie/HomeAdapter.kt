package app.example.merqueotest.ui.adapter.Movie

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.Toast
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import app.example.merqueotest.utils.EventBus.CountCarEvent
import app.example.merqueotest.R
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import com.bumptech.glide.Glide
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.cell_home_movie.view.*
import org.greenrobot.eventbus.EventBus

class HomeAdapter(val context: Context, val onClickListener: (movie: Movie, view: View) -> Unit): PagedListAdapter<Movie, HomeAdapter.MovieViewHolder>(
    DIFF_CALLBACK
) {

    private lateinit var CartDataSource: CartDataSource

    private val compositeDisposable : CompositeDisposable
    private val cartDataSource : CartDataSource
//    private val db by lazy { TmdbDatabase.getDB(context) }
//    private val cartDao by lazy { db.cartDao() }


    init {
        compositeDisposable = CompositeDisposable()
        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(context).cartDao())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.cell_home_movie, parent, false))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bindTo(holder, getItem(position))
    }

    inner class MovieViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val ivBackground: ImageView = view.imageViewBrackground
        private val imgAddShoppingCart = view.imgAddShoppingCart

        fun bindTo(vh: MovieViewHolder, movie: Movie?){
            Glide.with(context)
                .load(movie?.getPosterUrl())
                .placeholder(android.R.color.black)
                .into(ivBackground)

            movie?.let { item -> vh.itemView.setOnClickListener {

                it.transitionName = "poster"
                onClickListener(item, it)
            } }

            imgAddShoppingCart.setOnClickListener { saveShoppingCart(movie!!, imgAddShoppingCart) }
        }
    }

    private fun saveShoppingCart(
        movie: Movie,
        imgAddShoppingCart: ImageButton
    ) {

        val shoppingCart = ShoppingCart()
        shoppingCart.movieId = movie.id
        shoppingCart.imdbId = movie.imdbId
        shoppingCart.title = movie.title
        shoppingCart.adult = movie.adult
        shoppingCart.runtime = movie.runtime
        shoppingCart.voteCount = movie.voteCount
        shoppingCart.voteAverage = movie.voteAverage
        shoppingCart.popularity = movie.popularity
        shoppingCart.tagline = movie.tagline
        shoppingCart.posterPath = movie.posterPath
        shoppingCart.backdropPath = movie.backdropPath
        shoppingCart.overview = movie.overview
        shoppingCart.releaseDate = movie.releaseDate
        shoppingCart.page = movie.page
        shoppingCart.movieQuantity=1
        shoppingCart.alreadyAdded=true

//        imgAddShoppingCart.setImageDrawable(ResourcesCompat.getDrawable(context.resources, R.drawable.delete, null))

        compositeDisposable.add(cartDataSource.insertToShoppingCart(shoppingCart)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(context, "Add successful", Toast.LENGTH_SHORT).show()
                //Here we'll send a notify to MainActivity to update CounterFab
                EventBus.getDefault().postSticky(CountCarEvent(true))
            },{
                t: Throwable -> Toast.makeText(context, "[INSERT CART] $t", Toast.LENGTH_SHORT).show()
            }))
    }

    fun onStop(){
        if (compositeDisposable != null)
            compositeDisposable.clear()
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie) = oldItem.id == newItem.id
            override fun areContentsTheSame(oldItem: Movie, newItem: Movie) = oldItem == newItem
        }
    }
}