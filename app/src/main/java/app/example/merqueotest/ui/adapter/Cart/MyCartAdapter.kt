package app.example.merqueotest.ui.adapter.Cart

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import app.example.merqueotest.utils.EventBus.UpdateItemInCart
import app.example.merqueotest.R
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import com.bumptech.glide.Glide
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import io.reactivex.disposables.CompositeDisposable
import org.greenrobot.eventbus.EventBus
import java.lang.StringBuilder

class MyCartAdapter(internal var context: Context,
                    internal var cartItems: List<ShoppingCart>) :
    RecyclerView.Adapter<MyCartAdapter.MyViewHolder>() {

    internal var compositeDisposable: CompositeDisposable
    internal var cartDataSource: CartDataSource
    init {
        compositeDisposable = CompositeDisposable()
        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(context).cartDao())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_cart_item, parent, false))
    }

    override fun getItemCount(): Int {
        return cartItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        Glide.with(context).load(cartItems[position].getPosterUrl())
            .into(holder.imgCart)

        holder.tvMovieName.text = StringBuilder(cartItems[position].title!!)

        holder.numberButton.number = cartItems[position].movieQuantity.toString()

        //Event in ElegantButton


        holder.numberButton.setOnValueChangeListener{ _, _, newValue ->
            cartItems[position].movieQuantity = newValue
            EventBus.getDefault().postSticky(UpdateItemInCart(cartItems[position]))
        }
    }

    fun getItemAtPosition(pos: Int): ShoppingCart {
        return cartItems[pos]
    }


    inner class MyViewHolder(view: View): RecyclerView.ViewHolder(view) {

        lateinit var imgCart: ImageView
        lateinit var tvMovieName: TextView
        lateinit var numberButton: ElegantNumberButton

        init {
            imgCart = itemView.findViewById(R.id.imgMovieCart)
            tvMovieName = itemView.findViewById(R.id.tvMovieName)
            numberButton = itemView.findViewById(R.id.numberButton)
        }
    }
}