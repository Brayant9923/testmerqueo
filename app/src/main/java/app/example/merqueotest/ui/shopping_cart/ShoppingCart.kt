package app.example.merqueotest.ui.shopping_cart

import androidx.fragment.app.DialogFragment
import android.graphics.Bitmap
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.example.merqueotest.utils.EventBus.UpdateItemInCart
import app.example.merqueotest.R
import app.example.merqueotest.ui.adapter.Cart.MyCartAdapter
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.callback.IMyButtonCallBack
import app.example.merqueotest.ui.home.MainActivity
import app.example.merqueotest.ui.viewmodel.Cart.CartMovieModel
import app.example.merqueotest.utils.Common.MySwipeHelper
import app.example.merqueotest.utils.EventBus.CountCarEvent
import butterknife.ButterKnife
import butterknife.Unbinder
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.toolbar_shopping_cart.view.*
import kotlinx.android.synthetic.main.toolbar_shopping_cart.view.clearShoppingCart
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.lang.Exception


class ShoppingCart : DialogFragment() {

    //<editor-fold desc="Vars">

    val mainActivity =  MainActivity()
    val TAG = "dialog_shopping_caet"
    private var mBitmap: Bitmap? = null
    private var unbinder: Unbinder? = null

    private var recyclerViewState: Parcelable?=null

    private var cartDataSource: CartDataSource?=null
    private var compositeDisposable: CompositeDisposable?=null

    private lateinit var cartViewModel: CartMovieModel

    var linearEmptyCart: LinearLayout?=null
    var linearRecyclerCart: LinearLayout?=null
    var recyclerViewCart: RecyclerView?=null

    var adapter: MyCartAdapter?=null

    lateinit var activityMain: MainActivity


    //</editor-fold>

    //<editor-fold desc="Display">

        fun display(fragmentManager: FragmentManager, activity: MainActivity) {
            val exampleDialog = ShoppingCart()
            exampleDialog.show(fragmentManager, TAG)
            this.activityMain = activity
        }

    //</editor-fold>

    //<editor-fold desc="Life Cycle">

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_FullScreenDialog)
    }

    override fun onStart() {
        super.onStart()

        unbinder = ButterKnife.bind(this, dialog!!)

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)

        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            try {
                dialog.window!!.setLayout(width, height)
                dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
            } catch (e: Exception) {
                e.message
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        val view = inflater.inflate(R.layout.shopping_cart, container, false)

        view.imgCloseCart.setOnClickListener{ dismiss() }

        view.clearShoppingCart.setOnClickListener( clearShoppingCart)

        cartViewModel = ViewModelProviders.of(this).get(CartMovieModel::class.java)

        cartViewModel.initCartDataSource(context!!)

        initView(view)

        cartViewModel.getMutableLiveDataCartItem().observe(this, Observer {
            if (it == null || it.isEmpty()){
                linearRecyclerCart!!.visibility = GONE
                linearEmptyCart!!.visibility = VISIBLE
            }else{
                linearRecyclerCart!!.visibility = VISIBLE
                linearEmptyCart!!.visibility = GONE

                adapter = MyCartAdapter(context!!, it)
                recyclerViewCart!!.adapter = adapter
            }
        })

        return view
    }

    private fun initView(view: View) {
        setHasOptionsMenu(true)

        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(context!!).cartDao())

        recyclerViewCart = view.findViewById(R.id.recyclerViewCart) as RecyclerView
        recyclerViewCart!!.setHasFixedSize(true)
        val layoutManager = LinearLayoutManager(context)

        recyclerViewCart!!.layoutManager = layoutManager
        recyclerViewCart!!.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))

        val swipe = object:MySwipeHelper(context!!, recyclerViewCart!!, 200){
            override fun instantiateMyButton(
                viewHolder: RecyclerView.ViewHolder,
                buffer: MutableList<MyButton>
            ) {
                buffer.add(MyButton(context!!, resources.getString(R.string.delete),
                    30,
                    0,
                    Color.parseColor("#FF3C30"),
                    object :IMyButtonCallBack {
                        override fun onClick(pos: Int) {
                            val deleteItem = adapter!!.getItemAtPosition(pos)
                            cartDataSource!!.deleteAllShoppingCart(deleteItem)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(object:SingleObserver<Int>{
                                    override fun onSuccess(t: Int) {
                                        adapter!!.notifyItemRemoved(pos)
                                        Toast.makeText(context!!, resources.getString(R.string.delete_item_success), Toast.LENGTH_SHORT).show()
                                        EventBus.getDefault().postSticky(CountCarEvent(true))
                                    }

                                    override fun onSubscribe(d: Disposable) {

                                    }

                                    override fun onError(e: Throwable) {
                                        Toast.makeText(context!!, ""+e.message, Toast.LENGTH_SHORT).show()
                                    }

                                })
                        }
                    }))
            }
        }
        linearEmptyCart = view.findViewById(R.id.linearEmptyCart)
        linearRecyclerCart = view.findViewById(R.id.linearRecyclerCart)

    }

    override fun onResume() {
        super.onResume()
        //This is to lock the rotation
        try {
            activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } catch (e: NullPointerException) {
            e.message
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        unbinder!!.unbind()
        try {
            activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        } catch (e: NullPointerException) {
            e.message
        }

    }

    override fun onStop() {
        super.onStop()
        if (compositeDisposable != null)
            compositeDisposable!!.clear()

        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this)

        cartViewModel.onStop()
    }

    //</editor-fold>

    //<editor-fold desc="OnClick">

//    fun onHomeCategoryTemplate(templateThumbnail: TemplateThumbnail) {
//        val intent = Intent(context, MainActivity::class.java)
//        intent.putExtra("category", templateThumbnail.getCategory())
//        intent.putExtra("templateId", templateThumbnail.getId())
//        intent.putExtra("orientation", templateThumbnail.getOrientation())
//        intent.putExtra("proportion", templateThumbnail.getProportion())
//        intent.putExtra("using_template", true)
//        startActivity(intent)
//    }


//    fun startActivity(templateThumbnail: TemplateThumbnail) {
//        val intent = Intent(context, MainActivity::class.java)
//        intent.putExtra("category", templateThumbnail.getCategory())
//        intent.putExtra("templateId", templateThumbnail.getId())
//        intent.putExtra("orientation", templateThumbnail.getOrientation())
//        intent.putExtra("proportion", templateThumbnail.getProportion())
//        intent.putExtra("using_template", true)
//        startActivity(intent)
//        dismiss()
//    }

    //</editor-fold>

    //<editor-fold desc="Method from DB">

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onUpdateItemInCart(event: UpdateItemInCart){
        if (event.cartItem != null){
            recyclerViewState = recyclerViewCart!!.layoutManager!!.onSaveInstanceState()

            cartDataSource!!.updateCart(event.cartItem)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object:SingleObserver<Int>{
                    override fun onSuccess(t: Int) {
                        recyclerViewCart!!.layoutManager!!.onRestoreInstanceState(recyclerViewState)

                        EventBus.getDefault().postSticky(CountCarEvent(true))
                    }

                    override fun onSubscribe(dz: Disposable) {
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(context!!, "[UPDATE CART] " + e.message, Toast.LENGTH_LONG).show()
                    }

                })
        }
    }

    val clearShoppingCart =  View.OnClickListener { view ->
        cartDataSource!!.cleanShoppingCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Int> {
                override fun onSuccess(t: Int) {
                    Toast.makeText(context, resources.getString(R.string.clean_cart), Toast.LENGTH_SHORT).show()
                    EventBus.getDefault().postSticky(CountCarEvent(true))
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                    Toast.makeText(context, ""+e.message, Toast.LENGTH_SHORT).show()
                }

            })
    }

    //</editor-fold>
}