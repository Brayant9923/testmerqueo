package app.example.merqueotest.ui.viewmodel.Detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.Movie.Video
import app.example.merqueotest.repository.repository.TmdbRepository

class MovieDetailViewModel(application: Application, movie: Movie) : AndroidViewModel(application){
    private val repository: TmdbRepository =
        TmdbRepository(application)
    var movieDetail: LiveData<Movie>
    var movieVideos: LiveData<ArrayList<Video>>

    init {
        movieDetail = repository.requestMovieDetail(movie.id)
        movieVideos = repository.requestMovieVideos(movie.id)
    }
}