package app.example.merqueotest.ui.home

import android.app.SearchManager
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager2.widget.ViewPager2
import app.example.merqueotest.utils.EventBus.CountCarEvent
import app.example.merqueotest.R
import app.example.merqueotest.ui.adapter.Movie.HomeFragmentStateAdapter
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.Data.model.Movie.MovieType
import app.example.merqueotest.ui.shopping_cart.ShoppingCart
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.fabShoppingCart
import kotlinx.android.synthetic.main.toolbar.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MainActivity : AppCompatActivity() {


    //<editor-fold desc="Vars">

    private lateinit var cartDataSource : CartDataSource

    //</editor-fold>

    //<editor-fold desc="Life Cycle">

    override fun onStart() {
        super.onStart()
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(this@MainActivity).cartDao())

        fabShoppingCart.setOnClickListener { val shoppingCart = ShoppingCart()
            shoppingCart.display(supportFragmentManager, this) }

        setupToolbar()
        setupUI()
        countCartItem()
    }

    override fun onResume() {
        super.onResume()
        countCartItem()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    //</editor-fold>

    //<editor-fold desc="Menu Options">

    override fun onBackPressed() {
        if(viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_home, menu)

        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.action_search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
            maxWidth = Integer.MAX_VALUE
        }
        return true
    }

    private fun setupToolbar(){
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    //</editor-fold>

    //<editor-fold desc="SetupUI">

     private fun setupUI(){
        val adapter = HomeFragmentStateAdapter(this)
        MovieType.values().forEach {
            adapter.addFragment(HomeFragmentSlide(it))
        }

        viewPager.adapter = adapter
        viewPager.setPageTransformer(ZoomOutPageTransformer())
        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                tabLayout.setScrollPosition(position, 0F, true)
            }
        })

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
                tab.text = adapter.getFragmentTitle(position)
                tab.setIcon(adapter.getFragmentIcon(position))
        }.attach()
    }

    //</editor-fold>

    //<editor-fold desc="Save data of shopping">

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onCountCartEvent(event: CountCarEvent){
        if (event.isSuccess){
            countCartItem()
        }
    }

    private fun countCartItem() {
        cartDataSource.countItemInCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: SingleObserver<Int> {
                override fun onSuccess(t: Int) {
                    fabShoppingCart.count = t
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    if (!e.message!!.contains("Query returned empty")) {
                        Log.w("MAINACTIVITY", "ERROR : " + e.message)
                        Toast.makeText(this@MainActivity, " [COUNT CART] ", Toast.LENGTH_SHORT)
                            .show()
                    }else
                        fabShoppingCart.count = 0
                }

            })
    }


    //</editor-fold>
}