package app.example.merqueotest.ui.viewmodel.Movie

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.Movie.MovieType
import app.example.merqueotest.repository.repository.TmdbRepository

class MovieViewModel(application: Application): AndroidViewModel(application) {
    private val repository: TmdbRepository =
        TmdbRepository(application)
    private val moviesPopular: LiveData<PagedList<Movie>>
    private val moviesTopRated: LiveData<PagedList<Movie>>
    private val moviesUpcoming: LiveData<PagedList<Movie>>

    init {
        moviesPopular = repository.movieListPopular
        moviesTopRated = repository.movieListTopRated
        moviesUpcoming = repository.movieListUpcoming
    }

    fun getMovieList(movieType: MovieType): LiveData<PagedList<Movie>> {
        return when(movieType) {
            MovieType.POPULAR -> moviesPopular
            MovieType.TOP_RATED -> moviesTopRated
            MovieType.UPCOMING -> moviesUpcoming
        }
    }
}