package app.example.merqueotest.ui.viewmodel.Cart

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class CartMovieModel : ViewModel() {

    private var compositeDisposable: CompositeDisposable
    private var cartDataSource: CartDataSource?=null
    private var mutableLiveDataCartItem: MutableLiveData<List<ShoppingCart>>? = null

    init {
        compositeDisposable = CompositeDisposable()
    }

    fun initCartDataSource(context: Context){
        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(context).cartDao())
    }

    fun getMutableLiveDataCartItem():MutableLiveData<List<ShoppingCart>> {
        if (mutableLiveDataCartItem == null)
            mutableLiveDataCartItem = MutableLiveData()

        getCartItems()
        return mutableLiveDataCartItem!!

    }

    private fun getCartItems() {
        compositeDisposable.addAll(cartDataSource!!.loadAllShoppingCart()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({cartItems ->
                    mutableLiveDataCartItem!!.value = cartItems
            }, { t: Throwable? ->  mutableLiveDataCartItem!!.value = null })
        )
    }

    fun onStop(){
        compositeDisposable.clear()
    }
}