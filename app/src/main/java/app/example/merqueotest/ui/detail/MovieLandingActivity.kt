package app.example.merqueotest.ui.detail

import android.content.pm.ActivityInfo
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import app.example.merqueotest.Data.Room.ShoppingCart.CartDataSource
import app.example.merqueotest.Data.Room.ShoppingCart.LocalCartDataSource
import app.example.merqueotest.Data.Room.TmdbDatabase
import app.example.merqueotest.R
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.Movie.Video
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import app.example.merqueotest.utils.NavigationManager
import app.example.merqueotest.utils.NavigationManager.PARAM_CONTENT
import app.example.merqueotest.ui.viewmodel.Detail.MovieDetailViewModel
import app.example.merqueotest.ui.viewmodel.Detail.MovieDetailViewModelFactory
import app.example.merqueotest.utils.EventBus.CountCarEvent
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_landing_movie.*
import kotlinx.android.synthetic.main.toolbar.*
import org.greenrobot.eventbus.EventBus
import kotlin.collections.ArrayList

class MovieLandingActivity : AppCompatActivity() {

    //<editor-fold desc="Vars"

    private lateinit var viewModel: MovieDetailViewModel
    private var compositeDisposable: CompositeDisposable = CompositeDisposable()

    lateinit var movieTemp: Movie

    private var cartDataSource: CartDataSource?=null

    //</editor-fold>

    //<editor-fold desc="Life Cycle"
    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing_movie)

        cartDataSource = LocalCartDataSource(TmdbDatabase.getDB(this).cartDao())

        movieTemp = intent.getSerializableExtra(PARAM_CONTENT) as Movie

        floatingButtonShoppingCart.setOnClickListener { saveShoppingCart(movieTemp) }

        setupToolbar()
        loadData()
    }

    //</editor-fold>

    //<editor-fold desc="Setup View"
    private fun loadData(){

        setupUI(movieTemp) //Temporal data
        viewModel = ViewModelProviders.of(this,
            MovieDetailViewModelFactory(application, movieTemp)
        ).get(MovieDetailViewModel::class.java)
        viewModel.movieDetail.observe(this, Observer { setupUI(it) })
        viewModel.movieVideos.observe(this, Observer { setupBanner(it) })
    }

    private fun setupToolbar(){
        setSupportActionBar(toolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun setupUI(movie: Movie){
        Glide.with(this)
            .load(movie.getBackdropUrl())
            .placeholder(R.color.colorPrimary)
            .listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean { return false }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    gradientBanner.visibility = View.VISIBLE
                    return false
                }
            })
            .into(imageViewBanner)

        Glide.with(this)
            .load(movie.getPosterUrl())
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(android.R.color.black)
            .into(imageViewPoster)

        textViewTitle.text = movie.title
        textViewVotes.text = movie.getLikes()
        textViewStars.text = movie.getStars()
        textViewDate.text = movie.getYear()
        textViewDescription.text = movie.overview

        if(movie.tagline?.isNotEmpty() == true){
            textViewTagline.text = movie.tagline
            textViewTagline.visibility = View.VISIBLE
        } else {
            textViewTagline.visibility = View.GONE
        }
    }

    private fun setupBanner(videos: ArrayList<Video>){
        if(videos.isNotEmpty() && !isDestroyed && !isFinishing){

            var video = videos.find { it.isTrailer() && it.urlPlayback != null}
            if (video == null) {
                video = videos.find { it.urlPlayback?.isNotBlank() == true}
            }

            video?.let { item ->
                image_view_play.visibility = View.VISIBLE
                imageViewBanner.setOnClickListener {
                    NavigationManager.handle(this, item)
                }
            }
        }
    }

//    private fun setupVideos(){
//        val adapter = LandingMovieAdapter(this) {
//            NavigationManager.handle(this, it)
//        }
//
//        recyclerView.adapter = adapter
//        recyclerView.isNestedScrollingEnabled = false
//        recyclerView.layoutManager = LinearLayoutManager(this)
//        viewModel.movieVideos.observe(this, Observer {
//            adapter.submitList(it)
//            adapter.notifyDataSetChanged()
//        })
//    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    //</editor-fold>

    //<editor-fold desc="Save data local">
    private fun saveShoppingCart(
        movie: Movie) {

        val shoppingCart = ShoppingCart()
        shoppingCart.movieId = movie.id
        shoppingCart.imdbId = movie.imdbId
        shoppingCart.title = movie.title
        shoppingCart.adult = movie.adult
        shoppingCart.runtime = movie.runtime
        shoppingCart.voteCount = movie.voteCount
        shoppingCart.voteAverage = movie.voteAverage
        shoppingCart.popularity = movie.popularity
        shoppingCart.tagline = movie.tagline
        shoppingCart.posterPath = movie.posterPath
        shoppingCart.backdropPath = movie.backdropPath
        shoppingCart.overview = movie.overview
        shoppingCart.releaseDate = movie.releaseDate
        shoppingCart.page = movie.page
        shoppingCart.movieQuantity = 1
        shoppingCart.alreadyAdded = true

//        imgAddShoppingCart.setImageDrawable(ResourcesCompat.getDrawable(context.resources, R.drawable.delete, null))

        compositeDisposable.add(cartDataSource!!.insertToShoppingCart(shoppingCart)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(this, "Add successful", Toast.LENGTH_SHORT).show()
                //Here we'll send a notify to MainActivity to update CounterFab
                EventBus.getDefault().postSticky(CountCarEvent(true))
            }, { t: Throwable ->
                Toast.makeText(this, "[INSERT CART] $t", Toast.LENGTH_SHORT).show()
            })
        )
    }
    //</editor-fold>
}