package app.example.merqueotest.repository.repository

import androidx.paging.PagedList
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.Movie.MovieType

class MovieBoundaryCallback(private val repository: TmdbRepository, private val movieType: MovieType) : PagedList.BoundaryCallback<Movie>() {

    override fun onZeroItemsLoaded() {
        repository.requestMovies(1, movieType)
    }

    override fun onItemAtEndLoaded(itemAtEnd: Movie) {
        repository.requestMovies(itemAtEnd.page + 1, movieType)
    }
}