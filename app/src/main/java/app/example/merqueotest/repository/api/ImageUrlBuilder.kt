package app.example.merqueotest.repository.api

import app.example.merqueotest.BuildConfig

object ImageUrlBuilder {
    fun getUrl(path: String?, size: String = "w500"): String {
        if(path==null || path.isEmpty()) return ""

        return "${BuildConfig.IMAGE_URL}$size$path"
    }
}