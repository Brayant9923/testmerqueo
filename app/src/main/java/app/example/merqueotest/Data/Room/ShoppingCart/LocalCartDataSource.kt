package app.example.merqueotest.Data.Room.ShoppingCart

import androidx.lifecycle.LiveData
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class LocalCartDataSource(private val cartDao: CartDao) : CartDataSource {
    override fun cleanShoppingCart(): Single<Int> {
        return cartDao.cleanShoppingCart()
    }

    override fun updateCart(cart: ShoppingCart): Single<Int> {
        return cartDao.updateShoppingCart(cart)
    }

    override fun loadAllShoppingCart(): Flowable<List<ShoppingCart>> {
        return cartDao.loadAllShoppingCart()
    }

    override fun loadShoppingCart(movieId: String): LiveData<ShoppingCart> {
        return cartDao.loadShoppingCart(movieId)
    }

    override fun insertToShoppingCart(vararg movies: ShoppingCart): Completable {
        return cartDao.insertToShoppingCart(*movies)
    }

    override fun deleteAllById(movieId: String): Single<Int> {
        return cartDao.deleteAllById(movieId)
    }

    override fun deleteAllShoppingCart(shoppingCart: ShoppingCart): Single<Int> {
        return cartDao.deleteAllShoppingCart(shoppingCart)
    }

    override fun countItemInCart(): Single<Int> {
        return cartDao.countItemInCart()
    }
}