package app.example.merqueotest.Data.Room.Movie

import androidx.room.TypeConverter
import app.example.merqueotest.Data.model.Movie.MovieType

class MovieTypeConverter {
    @TypeConverter
    fun toMovieType(value: Int) = MovieType.get(value)

    @TypeConverter
    fun toInt(movieType: MovieType) = movieType.type
}