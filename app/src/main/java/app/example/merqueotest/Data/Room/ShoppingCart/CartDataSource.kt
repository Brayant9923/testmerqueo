package app.example.merqueotest.Data.Room.ShoppingCart

import androidx.lifecycle.LiveData
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface CartDataSource {

    fun loadShoppingCart(movieId: String): LiveData<ShoppingCart>

    fun loadAllShoppingCart(): Flowable<List<ShoppingCart>>

    fun insertToShoppingCart(vararg movies: ShoppingCart): Completable

    fun deleteAllById(movieId: String): Single<Int>

    fun deleteAllShoppingCart(shoppingCart: ShoppingCart): Single<Int>

    fun cleanShoppingCart(): Single<Int>

    fun countItemInCart(): Single<Int>

    fun updateCart(cart: ShoppingCart): Single<Int>

}