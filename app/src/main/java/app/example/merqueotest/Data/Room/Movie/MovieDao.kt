package app.example.merqueotest.Data.Room.Movie

import androidx.paging.DataSource
import androidx.room.*
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.Movie.MovieType

@Dao
interface MovieDao {

    @Query("SELECT * FROM movies WHERE type =:movieType ORDER BY page ASC")
    fun loadMovies(movieType: MovieType): DataSource.Factory<Int, Movie>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovies(movies: List<Movie>)

    @Insert
    fun insertMovie(movie: Movie)

    @Delete
    fun deleteMovie(movie: Movie)

    @Update
    fun updateMovie(movie: Movie)
}