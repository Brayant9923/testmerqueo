package app.example.merqueotest.Data.Room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import app.example.merqueotest.Data.Room.Movie.MovieDao
import app.example.merqueotest.Data.Room.Movie.MovieTypeConverter
import app.example.merqueotest.Data.Room.ShoppingCart.CartDao
import app.example.merqueotest.Data.model.Movie.Movie
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart

@Database(entities = [Movie::class, ShoppingCart::class], version = 6, exportSchema = false)
@TypeConverters(MovieTypeConverter::class)
abstract class TmdbDatabase : RoomDatabase() {
    abstract fun moviesDao(): MovieDao
    abstract fun cartDao(): CartDao

    companion object {
        @Volatile
        private var INSTANCE: TmdbDatabase? = null

        fun getDB(context: Context): TmdbDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    TmdbDatabase::class.java,
                "tmdb"
                ).fallbackToDestructiveMigration()
                .build()

                INSTANCE = instance
                return instance
            }
        }
    }
}