package app.example.merqueotest.Data.Room.ShoppingCart

import androidx.lifecycle.LiveData
import androidx.room.*
import app.example.merqueotest.Data.model.ShoppingCart.ShoppingCart
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface CartDao {

    @Query("SELECT * FROM ShoppingCart WHERE movieId=:movieId")
    fun loadShoppingCart(movieId: String): LiveData<ShoppingCart>

    @Query("SELECT * FROM ShoppingCart")
    fun loadAllShoppingCart(): Flowable<List<ShoppingCart>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertToShoppingCart(vararg cart: ShoppingCart): Completable

    @Query("DELETE FROM ShoppingCart WHERE movieId=:movieId")
    fun deleteAllById(movieId: String):Single<Int>

    @Query("DELETE FROM ShoppingCart")
    fun cleanShoppingCart():Single<Int>

    @Delete
    fun deleteAllShoppingCart(shoppingCart: ShoppingCart): Single<Int>

//    @Query("SELECT COUNT(*) FROM ShoppingCart")
//    fun countItemInCart(): Single<Int>

    @Query("SELECT SUM(movieQuantity) FROM ShoppingCart")
    fun countItemInCart(): Single<Int>

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateShoppingCart(cart: ShoppingCart): Single<Int>
}