package app.example.merqueotest.Data.model.ShoppingCart

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import app.example.merqueotest.repository.api.ImageUrlBuilder

@Entity(tableName = "ShoppingCart")
class ShoppingCart {

    @PrimaryKey
    @NonNull
    @ColumnInfo
    var movieId:Int?=-1

    @ColumnInfo
    var imdbId: String? = ""
    
    @ColumnInfo
    var adult: Boolean? = true
    
    @ColumnInfo
    var runtime: Int? = 0
    
    @ColumnInfo
    var voteCount: Int? =0

    @ColumnInfo
    var voteAverage: Double? = 0.0

    @ColumnInfo
    var popularity: Double? = 0.0

    @ColumnInfo
    var title: String? = ""

    @ColumnInfo
    var tagline: String? = ""

    @ColumnInfo
    var posterPath: String? = ""

    @ColumnInfo
    var backdropPath: String? = ""

    @ColumnInfo
    var overview: String? = ""

    @ColumnInfo
    var releaseDate: String? = ""

    @ColumnInfo
    var page: Int? = -1

    @ColumnInfo
    var movieQuantity: Int? = -1

    @ColumnInfo
    var alreadyAdded: Boolean? = false

    fun getPosterUrl() = ImageUrlBuilder.getUrl(posterPath)
}