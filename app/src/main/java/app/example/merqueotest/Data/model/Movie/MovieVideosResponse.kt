package app.example.merqueotest.Data.model.Movie

import com.google.gson.annotations.SerializedName

class MovieVideosResponse (
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val videos: ArrayList<Video>
)